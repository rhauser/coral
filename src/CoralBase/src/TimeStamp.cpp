// Include files
#include <climits>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <sstream>
#include "CoralBase/Exception.h"
#include "CoralBase/TimeStamp.h"

// Alpha and Omega
namespace coral
{
#if ((ULONG_MAX) == (UINT_MAX)) || defined ( __APPLE__ )
  // Beg of time for 32bit TimeStamp: 1901-12-13_20:45:52.000000000
  // End of time for 32bit TimeStamp: 2038-01-19_03:14:07.999999999
  // NB: Use 32bit TimeStamp on MacOSX due to timegm issues (CORALCOOL-2763)
  const TimeStamp::ValueType nsAlpha( INT_MIN * 1000000000LL ); // -2147483648000000000
  const TimeStamp::ValueType nsOmega( (-nsAlpha) - 1LL );       // +2147483647999999999
#else
  // Beg of time for 64bit TimeStamp: 1677-09-21_00:12:43.145224192
  // End of time for 64bit TimeStamp: 2262-04-11_23:47:16.854775807
  const TimeStamp::ValueType nsAlpha( LONG_LONG_MIN ); // -9223372036854775808
  const TimeStamp::ValueType nsOmega( LONG_LONG_MAX ); // +9223372036854775807
#endif
}

coral::TimeStamp::TimeStamp()
  : coral::TimeStamp( std::chrono::system_clock::now() )
{
}

coral::TimeStamp::TimeStamp( int year, int month, int day,
                             int hour, int minute, int second, long nanosecond )
  : m_year( year )
  , m_mon( month )
  , m_day( day )
  , m_hour( hour )
  , m_min( minute )
  , m_sec( second )
  , m_nsec( nanosecond )
{
  const TimeStamp alpha( nsAlpha );
  const TimeStamp omega( nsOmega );
  // Check input parameter ranges
  if ( month < 1 || month > 12 ||
       day < 1 || day > 31 ||
       hour < 0 || hour > 23 ||
       minute < 0 || minute > 59 ||
       second < 0 || second > 59 ||
       nanosecond < 0 || nanosecond > 999999999 )
  {
    std::stringstream s;
    s << "Invalid arguments to TimeStamp("
      << year << "," << month << "," << day << ","
      << hour << "," << minute << "," << second << "," << nanosecond << ")";
    throw Exception( s.str(), "TimeStamp::TimeStamp", "coral::CoralBase" );
  }
  if ( year < alpha.year() || year > omega.year() )
  {
    std::stringstream s;
    s << "Invalid TimeStamp("
      << year << "," << month << "," << day << ","
      << hour << "," << minute << "," << second << "," << nanosecond
      << ") outside [" << alpha << "," << omega << "]";
    throw Exception( s.str(), "TimeStamp::TimeStamp", "coral::CoralBase" );
  }
  // Check borderline cases
  if ( ( year == alpha.year() && ( *this ) < alpha ) ||
       ( year == omega.year() && ( *this ) > omega ) )
  {
    std::stringstream s;
    s << "Invalid TimeStamp("
      << year << "," << month << "," << day << ","
      << hour << "," << minute << "," << second << "," << nanosecond
      << ") outside [" << alpha << "," << omega << "]";
    throw Exception( s.str(), "TimeStamp::TimeStamp", "coral::CoralBase" );
  }
  // Sanity check (needed for months with fewer days but always executed)
  TimeStamp ts( this->total_nanoseconds() );
  if ( *this != ts )
  {
    std::stringstream s;
    s << "Invalid TimeStamp("
      << year << "," << month << "," << day << ","
      << hour << "," << minute << "," << second << "," << nanosecond
      << ") representing " << this->total_nanoseconds()
      << "ns i.e. " << ts;
    throw Exception( s.str(), "TimeStamp::TimeStamp", "coral::CoralBase" );
  }
}

coral::TimeStamp::TimeStamp( coral::TimeStamp::ValueType nsFromEpoch )
{
  // Use gmtime to convert nsFromEpoch into YYYY-MM-DD:hh:mm:ss.nnnnnnnnn
  ValueType secFromEpochLL = nsFromEpoch / 1000000000LL;
  m_nsec = nsFromEpoch % 1000000000LL;
  if ( nsFromEpoch < 0 && m_nsec != 0 )
  {
    secFromEpochLL -= 1;
    m_nsec += 1000000000LL;
  }
#if ((ULONG_MAX) == (UINT_MAX)) || defined ( __APPLE__ )
  // Restrict the range to 1901-2038 (fix CORALCOOL-2749 aka Y2038 bug...)
  if ( secFromEpochLL < INT_MIN || secFromEpochLL > INT_MAX )
  {
    std::stringstream s;
    s << "Invalid TimeStamp(" << nsFromEpoch
      << ") outside [" << nsAlpha << "," << nsOmega << "]";
    throw Exception( s.str(), "TimeStamp::TimeStamp", "coral::CoralBase" );
  }
#endif
  std::time_t secFromEpoch = secFromEpochLL; // this was causing CORALCOOL-2749
  struct std::tm t;
  ::gmtime_r( &secFromEpoch, &t );
  m_year = t.tm_year + 1900;
  m_mon  = t.tm_mon + 1;
  m_day  = t.tm_mday;
  m_hour = t.tm_hour;
  m_min  = t.tm_min;
  m_sec  = t.tm_sec;
}

coral::TimeStamp::ValueType coral::TimeStamp::total_nanoseconds() const
{
  // Use "timegm" to convert YYYY-MM-DD:hh:mm:ss.nnnnnnnnn into nsFromEpoch
  struct std::tm t = { m_sec, m_min, m_hour, m_day, m_mon-1, m_year-1900, 0, 0, -1, 0, NULL };
  std::time_t secFromEpoch = ::timegm(&t);
  //std::cout << "_DEBUG " << secFromEpoch << ", " << m_nsec << std::endl;
  return secFromEpoch*1000000000LL + m_nsec;
}

coral::TimeStamp coral::TimeStamp::now()
{
  return coral::TimeStamp();
}

std::string coral::TimeStamp::toString() const
{
  std::stringstream ss;
  this->print( ss );
  return ss.str();
}

coral::TimeStamp::TimeStamp( const std::chrono::system_clock::time_point& tp )
  : TimeStamp( (std::chrono::duration_cast<std::chrono::nanoseconds>( tp.time_since_epoch() ) ).count() )
{
}

coral::TimeStamp::~TimeStamp()
{
}

coral::TimeStamp::TimeStamp( const coral::TimeStamp& rhs )
  : m_year( rhs.m_year )
  , m_mon( rhs.m_mon )
  , m_day( rhs.m_day )
  , m_hour( rhs.m_hour )
  , m_min( rhs.m_min )
  , m_sec( rhs.m_sec )
  , m_nsec( rhs.m_nsec )
{
}

coral::TimeStamp&
coral::TimeStamp::operator=( const coral::TimeStamp& rhs )
{
  if( this != &rhs )
  {
    m_year = rhs.m_year;
    m_mon = rhs.m_mon;
    m_day = rhs.m_day;
    m_hour = rhs.m_hour;
    m_min = rhs.m_min;
    m_sec = rhs.m_sec;
    m_nsec = rhs.m_nsec;
  }
  return *this;
}

int
coral::TimeStamp::year() const
{
  return m_year;
}

int
coral::TimeStamp::month() const
{
  return m_mon;
}

int
coral::TimeStamp::day() const
{
  return m_day;
}

int
coral::TimeStamp::hour() const
{
  return m_hour;
}

int
coral::TimeStamp::minute() const
{
  return m_min;
}

int
coral::TimeStamp::second() const
{
  return m_sec;
}

long
coral::TimeStamp::nanosecond() const
{
  return m_nsec;
}

std::chrono::system_clock::time_point
coral::TimeStamp::time() const
{
  std::chrono::nanoseconds nsFromEpoch( this->total_nanoseconds() );
  return std::chrono::system_clock::time_point( std::chrono::duration_cast<std::chrono::system_clock::duration>( nsFromEpoch ) );
}

bool
coral::TimeStamp::operator==( const coral::TimeStamp& rhs ) const
{
  return ( ( m_year == rhs.m_year ) &&
           ( m_mon == rhs.m_mon ) &&
           ( m_day == rhs.m_day ) &&
           ( m_hour == rhs.m_hour ) &&
           ( m_min == rhs.m_min ) &&
           ( m_sec == rhs.m_sec ) &&
           ( m_nsec == rhs.m_nsec ) );
}

bool
coral::TimeStamp::operator!=( const coral::TimeStamp& rhs ) const
{
  return !( *this == rhs );
}

// Implementation as in cool::Time
bool
coral::TimeStamp::operator>( const coral::TimeStamp& rhs ) const
{
  if ( m_year > rhs.m_year ) return true;
  if ( m_year < rhs.m_year ) return false;
  if ( m_mon > rhs.m_mon ) return true;
  if ( m_mon < rhs.m_mon ) return false;
  if ( m_day > rhs.m_day ) return true;
  if ( m_day < rhs.m_day ) return false;
  if ( m_hour > rhs.m_hour ) return true;
  if ( m_hour < rhs.m_hour ) return false;
  if ( m_min > rhs.m_min ) return true;
  if ( m_min < rhs.m_min ) return false;
  if ( m_sec > rhs.m_sec ) return true;
  if ( m_sec < rhs.m_sec ) return false;
  if ( m_nsec > rhs.m_nsec ) return true;
  else return false; // m_nsec <= rhs.m_nsec
}

// Implementation as in cool::ITime
bool
coral::TimeStamp::operator>=( const coral::TimeStamp& rhs ) const
{
  return ( (*this) > rhs || (*this) == rhs );
}

// Implementation as in cool::ITime
bool
coral::TimeStamp::operator<( const coral::TimeStamp& rhs ) const
{
  return !( (*this) >= rhs );
}

// Implementation as in cool::ITime
bool
coral::TimeStamp::operator<=( const coral::TimeStamp& rhs ) const
{
  return !( (*this) > rhs );
}

// Implementation as in cool::Time
std::ostream&
coral::TimeStamp::print( std::ostream& os ) const
{
  int year = this->year();
  int month = this->month(); // Months are in [1-12]
  int day = this->day();
  int hour = this->hour();
  int min = this->minute();
  int sec = this->second();
  long nsec = this->nanosecond();
  char timeChar[] = "yyyy-mm-dd_hh:mm:ss.nnnnnnnnn GMT";
  int nChar = ::strlen(timeChar);
  if ( ::snprintf( timeChar, nChar+1, // Fix Coverity SECURE_CODING
                   "%4.4d-%2.2d-%2.2d_%2.2d:%2.2d:%2.2d.%9.9ld GMT",
                   year, month, day, hour, min, sec, nsec) == nChar )
  {
    return os << std::string( timeChar );
  }
  // No path to this statement, normally...
  std::stringstream s;
  s << "coral::TimeStamp(" << year << "," << month << "," << day << ","
    << hour << "," << min << "," << sec << "," << nsec << ")";
  std::cout << "[***CORAL INTERNAL ERROR in " << s.str()
            << ": please report!***]" << std::endl; // gentler than an exception
  return os << s.str();
}
