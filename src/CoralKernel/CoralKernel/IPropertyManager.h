#ifndef IPROPERTYMANAGER_H_
#define IPROPERTYMANAGER_H_ 1

// Include files
#include <string>
#include "CoralKernel/IProperty.h"

namespace coral
{

  /**
   * @class IPropertyManager
   *
   * Interface for an object managing a set of properties. The object must
   * store a fixed property set, properties cannot be added/deleted by this
   * interface. The properties are identified by strings (names).
   *
   * @author Zsolt Molnar
   * @date   2008-05-21
   *///

  class IPropertyManager // class vs struct (WIN32 bug #63198, clang bug #79151)
  {

  public:

    virtual ~IPropertyManager() {}

    /**
     * Return the property identified by the given string or NULL if the
     * property does not exist.
     *///
    virtual IProperty* property(const std::string&) = 0;

  };

}
#endif /*IPROPERTYMANAGER_H_*///
