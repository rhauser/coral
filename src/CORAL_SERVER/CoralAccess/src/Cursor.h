#ifndef CORALACCESS_CURSOR_H
#define CORALACCESS_CURSOR_H 1

// Include files
#include <memory>
#include <vector>
#include "RelationalAccess/ICursor.h"
#include "CoralServerBase/ICoralFacade.h"
#include "CoralServerBase/QueryDefinition.h"

namespace coral
{

  namespace CoralAccess
  {

    // Forward declarations
    class Cursor;
    class SessionProperties;

    /** @class Cursor
     *
     *  @author Andrea Valassi
     *  @date   2007-12-05
     *///

    class Cursor : virtual public ICursor
    {

    public:

      /// Constructor
      Cursor( std::shared_ptr<const SessionProperties> sessionProperties,
              const QueryDefinition& queryDef,
              coral::AttributeList* pOutputBuffer,
              unsigned int rowCacheSize,
              unsigned int memoryCacheSize );

      /// Constructor
      Cursor( std::shared_ptr<const SessionProperties> sessionProperties,
              const QueryDefinition& queryDef,
              const std::map< std::string, std::string > outputTypes,
              unsigned int rowCacheSize,
              unsigned int memoryCacheSize );

      /// Destructor
      virtual ~Cursor();

      /**
       * Positions the cursor to the next available row in the result set.
       * If there are no more rows in the result set false is returned.
       *///
      bool next();

      /**
       * Returns a reference to output buffer holding the data of the last
       * row fetched.
       *///
      const AttributeList& currentRow() const;

      /**
       * Explicitly closes the cursor, releasing the resources on the server.
       *///
      void close();

      /// Returns the facade for the CORAL server connection.
      const ICoralFacade& facade() const;

    private:

      /// The properties of this remote database session.
      std::shared_ptr<const SessionProperties> m_sessionProperties;

      /// The row iterator.
      IRowIteratorPtr m_rows;

    };

  }

}
#endif // CORALACCESS_CURSOR_H
