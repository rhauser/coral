// Include files
#include "RelationalAccess/SessionException.h"

// Local include files
#include "ConnectionProperties.h"
#include "DomainProperties.h"
#include "Session.h"
#include "SessionProperties.h"

// Namespace
using namespace coral::CoralAccess;

//-----------------------------------------------------------------------------

SessionProperties::SessionProperties( std::shared_ptr<ConnectionProperties> connectionProperties,
                                      const std::string& databaseUrl,
                                      const coral::AccessMode accessMode,
                                      Session& session )
  : m_connectionProperties( connectionProperties )
  , m_domainServiceName( connectionProperties->domainProperties().service()->name() )
  , m_databaseUrl( databaseUrl )
  , m_accessMode( accessMode )
  , m_session( &session )
  , m_sessionID( 0 )
  , m_fromProxy( false )
  , m_isUserSessionActive( false )
  , m_remoteTechnologyName( "" )
  , m_remoteServerVersion( "" )
  , m_monitoringService( 0 )
{
}

//-----------------------------------------------------------------------------

SessionProperties::~SessionProperties()
{
}

//-----------------------------------------------------------------------------

void SessionProperties::nullifySession()
{
  m_session = NULL;
}

//-----------------------------------------------------------------------------

void
SessionProperties::setState( coral::Token sessionID,
                             bool fromProxy,
                             bool isUserSessionActive )
{
  m_sessionID = sessionID;
  m_fromProxy = fromProxy;
  m_isUserSessionActive = isUserSessionActive;
}

//-----------------------------------------------------------------------------

void
SessionProperties::setRemoteProperties( const std::string& remoteTechnologyName,
                                        const std::string& remoteServerVersion,
                                        const std::string& remoteNominalSchemaName )
{
  m_remoteTechnologyName = remoteTechnologyName;
  m_remoteServerVersion = remoteServerVersion;
  m_remoteNominalSchemaName = remoteNominalSchemaName;
}

//-----------------------------------------------------------------------------

/*
const ConnectionProperties& SessionProperties::connectionProperties() const
{
  return *m_connectionProperties;
}
*/

//-----------------------------------------------------------------------------

const coral::ICoralFacade& SessionProperties::facade() const
{
  return m_connectionProperties->facade();
}

//-----------------------------------------------------------------------------

const std::string& SessionProperties::databaseUrl() const
{
  return m_databaseUrl;
}

//-----------------------------------------------------------------------------

coral::AccessMode SessionProperties::accessMode() const
{
  return m_accessMode;
}

//-----------------------------------------------------------------------------

coral::Token SessionProperties::sessionID() const
{
  return m_sessionID;
}

//-----------------------------------------------------------------------------

bool SessionProperties::fromProxy() const
{
  return m_fromProxy;
}

//-----------------------------------------------------------------------------

bool SessionProperties::isUserSessionActive() const
{
  return m_isUserSessionActive;
}

//-----------------------------------------------------------------------------

const std::string& SessionProperties::remoteTechnologyName() const
{
  return m_remoteTechnologyName;
}

//-----------------------------------------------------------------------------

const std::string& SessionProperties::remoteServerVersion() const
{
  return m_remoteServerVersion;
}

//-----------------------------------------------------------------------------

const std::string& SessionProperties::remoteNominalSchemaName() const
{
  return m_remoteNominalSchemaName;
}

//-----------------------------------------------------------------------------

bool SessionProperties::isTransactionActive() const
{
  if ( !m_session )
    throw coral::SessionException( "Session has been nullified",
                                   "SessionProperties::isTransactionActive",
                                   domainServiceName() );
  return m_session->isTransactionActive();
}

//-----------------------------------------------------------------------------

const std::string&
SessionProperties::domainServiceName() const
{
  return m_domainServiceName;
}

//-----------------------------------------------------------------------------

std::string
SessionProperties::connectionString() const
{
  return m_connectionProperties->coralServerUrl() + "&" + databaseUrl();
}

//-----------------------------------------------------------------------------

void SessionProperties::setMonitoringService( monitor::IMonitoringService* monitoringService )
{
  m_monitoringService = monitoringService;
}

//-----------------------------------------------------------------------------

coral::monitor::IMonitoringService* SessionProperties::monitoringService() const
{
  return m_monitoringService;
}

//-----------------------------------------------------------------------------
