#ifndef CORALSERVERBASE_HEXSTRING_H
#define CORALSERVERBASE_HEXSTRING_H 1

// Include files
#include <iostream>
#include <string>

/** @file hexstring.h
 *
 *  Simple methods for converting a string into a hex dump.
 *
 *  @author Andrea Valassi
 *  @date   2009-01-21
 *///

namespace coral
{

  //-------------------------------------------------------------------------

  inline const std::string hexstring( const char* pData,
                                      size_t nData )
  {
    std::string hexData = "0x";
    for ( size_t p=0; p<nData; p++ )
    {
      const char* const hex = "0123456789ABCDEF";
      unsigned char cTmp = *(pData+p); // NB Need __unsigned__ char conversion!
      hexData += hex[(cTmp >> 4) & 0xF];
      hexData += hex[cTmp & 0xF];
    }
    return hexData;
  }

  //-------------------------------------------------------------------------

  inline const std::string hexstring( const unsigned char* pData,
                                      size_t nData )
  {
    return hexstring( (const char*) pData, nData );
  }

  //-------------------------------------------------------------------------

  inline const std::string hexstring( const std::string& data )
  {
    return hexstring( data.c_str(), data.size() );
  }

  //-------------------------------------------------------------------------

}
#endif // CORALSERVERBASE_HEXSTRING_H
