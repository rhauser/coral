
// Include files
#include "CoralKernel/CoralPluginDef.h"

// Local include files
#include "CoralServerFacadeService.h"

//-----------------------------------------------------------------------------

CORAL_PLUGIN_MODULE( "CORAL/Services/CoralServerFacade",
                     coral::CoralServer::CoralServerFacadeService )

//-----------------------------------------------------------------------------
