#! /bin/bash

which igprof > /dev/null 2>&1
if [ "$?" != "0" ]; then
  # igprof not found: try to take it from LCG_releases_base (CORALCOOL-2994)
  if [ -d ${LCG_releases_base}/igprof ] && [ -d ${LCG_releases_base}/libunwind ]; then
    igprofPath=`find ${LCG_releases_base}/igprof -mindepth 2 -maxdepth 2 -name ${BINARY_TAG}`
    unwindPath=`find ${LCG_releases_base}/libunwind -mindepth 2 -maxdepth 2 -name ${BINARY_TAG}`
    if [ -d ${igprofPath} ] && [ -d ${unwindPath} ]; then
      export PATH=${igprofPath}/bin:${PATH}
      export LD_LIBRARY_PATH=${igprofPath}/lib:${unwindPath}/lib:${LD_LIBRARY_PATH}
    fi
  fi 
  which igprof > /dev/null 2>&1
  if [ "$?" != "0" ]; then
    # igprof not found again: give up
    echo "ERROR! Unknown command igprof"
    exit 1
  else
    echo "WARNING! Path to igprof and libunwind is not set, try:"
    echo "export PATH=${igprofPath}/bin:\$PATH"
    echo "export LD_LIBRARY_PATH=${igprofPath}/lib:${unwindPath}/lib:\$LD_LIBRARY_PATH"
  fi  
fi

if [ "$1" == "-pp" ]; then
  prof=pp
  opts="-pp"
  shift
elif [ "$1" == "-pr" ]; then
  prof=pr
  opts="-pp -pr"
  shift
elif [ "$1" == "-pu" ]; then
  prof=pu
  opts="-pp -pu"
  shift
elif [ "$1" == "-mp" ]; then
  prof=mp
  opts="-mp"
  shift
else
  echo "Usage $0 (-pp|-pr|-pu|-mp) <executable> [<args>]"
  exit 1
fi

if [ "$1" == "" ]; then
  echo "Usage $0 (-pp|-pr|-pu|-mp) <executable> [<args>]"
  exit 1
fi
cmd=$1
shift

args="$*"
cmdlog=$cmd
while [ "$1" != "" ]; do cmdlog=${cmdlog}_${1//\//_}; shift; done
cmdlog=${cmdlog}.${BINARY_TAG}

which $cmd > /dev/null 2>&1
if [ "$?" != "0" ]; then
  echo "ERROR! Unknown command $cmd"
  exit 1
fi

logdir=/afs/cern.ch/sw/lcg/app/releases/CORAL/internal/igprof-www/cgi-bin/data/$USER
if [ ! -d $logdir ]; then
  echo "ERROR! $logdir does not exist"
  exit 1
fi

\rm -f $logdir/${cmdlog}.${prof}.gz
date
echo "igprof -d $opts -z -o $logdir/${cmdlog}.${prof}.gz $cmd ${args}"
time igprof -d $opts -z -o $logdir/${cmdlog}.${prof}.gz $cmd ${args}
date

pushd $logdir > /dev/null
echo "------"
if [ "$prof" == "mp" ]; then
  reps="MEM_LIVE MEM_TOTAL MEM_LIVE_PEAK MEM_MAX" 
  for rep in $reps; do 
    \rm -f ${cmdlog}.${rep}.sql3
    echo "igprof-analyse --sqlite -d -v -g -r $rep ${cmdlog}.${prof}.gz | sqlite3 ${cmdlog}.${rep}.sql3"
    igprof-analyse --sqlite -d -v -g -r $rep ${cmdlog}.${prof}.gz | sqlite3 ${cmdlog}.${rep}.sql3
    echo ""
    echo "------"
  done
else
  \rm -f ${cmdlog}.${prof}.PERF_TICKS.sql3
  echo "igprof-analyse --sqlite -d -v -g ${cmdlog}.${prof}.gz | sqlite3 ${cmdlog}.${prof}.PERF_TICKS.sql3"
  igprof-analyse --sqlite -d -v -g ${cmdlog}.${prof}.gz | sqlite3 ${cmdlog}.${prof}.PERF_TICKS.sql3
  echo ""
  echo "------"
fi
popd > /dev/null

echo "Output files in $logdir"
echo "Browse https://test-coral-igprof.web.cern.ch/test-coral-igprof/$USER/igprof-navigator-index.php"

