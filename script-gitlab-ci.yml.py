#!/usr/bin/env python3
def build_gitlabci(binary_tag, view, OS):
  return f"""build_{binary_tag}_{view}:
  variables:
    BINARY_TAG: {binary_tag}
  artifacts: 
    when: always
    expire_in: "3 mos"
    paths: 
      - $BINARY_TAG/
      - logs/
  script: "./cc-build -b $BINARY_TAG -v {view}/latest"
  stage: build
  tags: 
    - {OS}
test_{binary_tag}_{view}:
  variables:
    BINARY_TAG: {binary_tag}
  dependencies:
    - build_{binary_tag}_{view}
  artifacts: 
    when: always
    expire_in: "3 mos"
    paths: 
      - logs/qmtestCoral/$BINARY_TAG
  script: 
    - cd $BINARY_TAG/
    - "export CORAL_AUTH_PATH=/home/gitlab-runner/"
    - "export CORAL_DBLOOKUP_PATH=/home/gitlab-runner/"
    - "time ./cc-run ./qmtestRun.sh"
    - ../logs/qmtestCoral/summarizeAll.sh
    - "cd ../logs/qmtestCoral/"
    - "mkdir $BINARY_TAG"
    - "cp $BINARY_TAG.summary $BINARY_TAG.xml ./$BINARY_TAG"
    - "! grep -e FAIL -e ERROR $BINARY_TAG.summary"
  stage: test
  when: always
  tags: 
    - {OS}

"""

platforms=['dev3-x86_64-centos7-clang10-dbg', 'dev3-x86_64-centos7-clang10-opt', 'dev3-x86_64-centos7-gcc10-dbg', 'dev3-x86_64-centos7-gcc10-opt', 'dev3-x86_64-centos7-gcc8-dbg', 'dev3-x86_64-centos7-gcc8-opt', 'dev3-x86_64-centos7-gcc9-dbg', 'dev3-x86_64-centos7-gcc9-opt', 'dev3python2-x86_64-centos7-gcc8-opt', 'dev3python2-x86_64-centos7-gcc9-opt']
 
with open('.gitlab-ci.yml', 'w') as f:
  f.write("""---
stages: 
  - build
  - test

""")
  for platform in platforms:
    list = platform.split('-')
    f.write(build_gitlabci(platform[platform.find('-')+1:], list[0], list[2]))

